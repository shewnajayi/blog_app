Rails.application.routes.draw do
  # devise_for :users

  get 'users/signup', to: 'users#new'
  get 'users/login', to: 'sessions#new'
  post 'users/login', to: 'sessions#create'
  delete 'users/logout', to: 'sessions#destroy'

  root 'posts#index'
  resources :comments
  resources :posts
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
